package com.kstu;

import com.kstu.transport.Task;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

// Тест,который решает 5,6,7 варианты
public class TransportTaskTest {
    private Task task5;
    private Task task6;
    private Task task7;
    private Integer[][] rightTaskPlan5;
    private Integer[][] rightTaskPlan6;
    private Integer[][] rightTaskPlan7;


    @Before
    public void initialization() {

        //Вариант 5
        Integer[][] table5 = {
                {7, 12, 4, 6, 5},
                {1, 8, 6, 5, 3},
                {6, 13, 8, 7, 4}
        };

        task5 = new Task(new Integer[]{110, 90, 120, 80, 150}, new Integer[]{180, 350, 20}, table5);


        //Вариант 6
        Integer[][] table6 = {
                {2, 3, 4, 2, 4},
                {8, 4, 1, 4, 1},
                {9, 7, 3, 7, 2}
        };

        task6 = new Task(new Integer[]{60, 70, 120, 130, 100}, new Integer[]{140, 180, 160}, table6);


        //Вариант 7
        Integer[][] table7 = {
                {4, 5, 2, 8, 6},
                {3, 1, 9, 7, 3},
                {9, 6, 7, 2, 1}
        };

        task7 = new Task(new Integer[]{70, 220, 40, 30, 60}, new Integer[]{115, 175, 130}, table7);


        rightTaskPlan5 = new Integer[][]{
                {0, 0, 120, 60, 0},
                {110, 90, 0, 20, 130},
                {0, 0, 0, 0, 20}
        };

        rightTaskPlan6 = new Integer[][]{
                {60, 0, 0, 80, 0},
                {0, 70, 60, 50, 0},
                {0, 0, 60, 0, 100}
        };


        rightTaskPlan7 = new Integer[][]{
                {70, 5, 40, 0, 0},
                {0, 175, 0, 0, 0},
                {0, 40, 0, 30, 60}
        };


    }

    @Test
    public void transportTaskTest() {
        Assert.assertEquals(Integer.valueOf(2240), task5.runTask());
        Assert.assertEquals(Integer.valueOf(1200), task6.runTask());
        Assert.assertEquals(Integer.valueOf(920), task7.runTask());

        Assert.assertArrayEquals(rightTaskPlan5, task5.getPlan());
        Assert.assertArrayEquals(rightTaskPlan6, task6.getPlan());
        Assert.assertArrayEquals(rightTaskPlan7, task7.getPlan());

    }
}
