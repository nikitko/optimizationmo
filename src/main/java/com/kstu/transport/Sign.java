package com.kstu.transport;

public class Sign {

    private String sign = "+";

    public String getSign() {
        String val = sign;
        if (sign.equals("+")) sign = "-";
        else sign = "+";
        return val;
    }
}
